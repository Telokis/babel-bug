# Test repo to showcase strange behavior in Babel config handling

**EVERYTHING MUST BE DONE FROM WITHIN THE server DIRECTORY**. It's the heart of the issue. Everything else is just here for recreating my real environment.

## Available npm scripts:

-   `npm run basic` --> The command I expected to work without any workaround.
-   `npm run basic-with-only` --> The command that enables usage of babel plugins within common but doesn't handle typescript files (despite the `-x` option)
-   `npm run common` --> Directly tries to execute a js file located within common. (I expected this to work)
-   `npm run common-with-only` --> Executes the js file located within common properly but feels hacky
-   `npm run ts` --> Directly tries to execute a typescript file located within common. (I expected this to work)
-   `npm run ts-with-only` --> Doesn't work even though I see my `Inside root babel.config.js` log indicating proper config parsing. This is the real strange thing

## Stackoverflow question

I originally asked this on StackOverflow since I thought it was an error on my end. I'm not sure if it's my fault anymore (especially because of the `ts-with-only` not working).

https://stackoverflow.com/questions/55772699/make-a-babel-7-config-take-effect-in-a-sibling-directory

## Github issue

I created an issue in the babel repo to try and ask if it's a bug or not.

https://github.com/babel/babel/issues/9879

## Additionnal comments I made while searching

After taking a look at the babel code to find the config parsing, I noticed that this line : https://github.com/babel/babel/blob/8ca99b9f0938daa6a7d91df81e612a1a24b09d98/packages/babel-core/src/config/config-chain.js#L456 is called.  
After printing everything in this scope, I noticed that babel automatically generates a `only` parameter containing the cwd. (Effectively saying that my `babel.config.js` doesn't affect my `common` directory despite being "above" is in the directory hierarchy).  
I decided to try overloading it in the command line and arrived at this command: `npx babel-node src/index.js --root-mode upward -x .ts,.js --only .,../Common/ --ignore node_modules` (Added `--only` and `--ignore`)  
This made me progress a bit: instead of failing to parse advanced syntax (pipeline operator) **in js files**, it failed on a ts failing, saying

```
export const accountStatus = Object.freeze({
^^^^^^

SyntaxError: Unexpected token export
```

What I don't understand is how it can parse the pipeline operator but not the typescript file even though both the pipeline plugin and the typescript are inside the same `babel.config.js`
