console.log("Inside Server .babelrc.js");

module.exports = {
    presets: [
        [
            "@babel/preset-env",
            {
                targets: {
                    node: "current",
                },
                exclude: ["transform-for-of"],
            },
        ],
    ],
};
