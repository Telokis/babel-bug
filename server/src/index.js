console.log("Inside index.js");

function doubleSay(str) {
    return `${str}, ${str}`;
}

function capitalize(str) {
    return str[0].toUpperCase() + str.substring(1);
}

function exclaim(str) {
    return `${str}!`;
}

const result = "hello" |> doubleSay |> capitalize |> exclaim;

console.log("\tPipeline test result: ", result, "(should be 'Hello, hello!')");
console.log("\tLeaving index.js");

console.log("Requiring ../../common/pipeline");
require("../../common/pipeline");

console.log("Requiring ../../common/enum");
require("../../common/enum");
