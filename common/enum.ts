console.log("Inside enum.ts");

export const accountStatus = Object.freeze({
    pending: "pending",
    activated: "activated",
    banned: "banned",
    deactivated: "deactivated",
});

console.log("Leaving enum.ts");
